import { Injectable }   from '@angular/core';
import { HttpClient }   from '@angular/common/http';
import { Observable }   from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import { Branch } from '../models/branch.model';

@Injectable()
export class BranchesService
{
  private serviceUrl = 'http://localhost:8090/branches';
  
  constructor(private http: HttpClient) { }
  
  getBranch(): Observable<Branch[]> {
    return this.http.get<Branch[]>(this.serviceUrl);
  }
  

}
