export interface Branch
{
	branchId : number;
	address : string;
	city : string;
	name : string;
	state : string;
	zipCode : string;
}