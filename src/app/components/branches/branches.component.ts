import { Component, OnInit } from '@angular/core';
import { BranchesService } from '../../services/branches.service';

import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/of';
import {DataSource} from '@angular/cdk/collections';
import { Branch } from '../../models/branch.model';

@Component({
  selector: 'app-branches',
  templateUrl: './branches.component.html',
  styleUrls: ['./branches.component.scss'],
  providers: [BranchesService]
})

export class BranchesComponent implements OnInit
{
  dataSource = new UserDataSource(this.branchesService);
  displayedColumns = ['branchId', 'address', 'city', 'name', 'state' , 'zip', 'actionsColumn'];
  constructor(private branchesService : BranchesService) { }
  
  ngOnInit() 
  {
  }
  
  public details(branchId : number)
  {
	alert("You pressed: " + branchId);
  }
}


export class UserDataSource extends DataSource<any>
{
  constructor(private branchesService : BranchesService) 
  {
    super();
  }
  
  connect(): Observable<Branch[]> 
  {
    return this.branchesService.getBranch();
  }
  
  disconnect() {}
}
