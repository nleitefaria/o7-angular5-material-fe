import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule, HttpClient } from '@angular/common/http';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MaterialModule } from './material.module';

//Module
import { AppRoutingModule } from './app-routing.module';

//Component
import { AppComponent } from './app.component';
import { BranchesComponent } from './components/branches/branches.component';

//Service
import { BranchesService } from './services/branches.service';


@NgModule({
  declarations: [
    AppComponent,
    BranchesComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
	HttpClientModule,
	BrowserAnimationsModule,
    MaterialModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
